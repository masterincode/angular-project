# StackTech Project Management

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.1.4.

## To Open Project into VS COde

- Go to your project directory, open cmd , and run `code .` 

## Angular Commands

- `ng new --help`
- `ng build --help`

# Custom directive from default value `app` to `bms`, go to angualar.json -> "prefix" :"app"

    ng new stach-tech --prefix bms

## To create New Component without folder

    ng g c component_name --flat

## TO create new Gaurd

    ng g g gaurd_name

## To create child module and import into AppModule

    ng g m module_name -m app

## To create module and import into another Module

    ng g m shared --flat -m product.module.ts

